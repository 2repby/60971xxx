-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Дек 13 2020 г., 02:54
-- Версия сервера: 5.7.32-0ubuntu0.18.04.1
-- Версия PHP: 7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `curating`
--

-- --------------------------------------------------------

--
-- Структура таблицы `rating`
--

CREATE TABLE `rating` (
  `id` int(11) NOT NULL COMMENT 'Ключ',
  `name` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'Имя персоны',
  `description` text CHARACTER SET utf8 NOT NULL COMMENT 'Описание',
  `gender` int(11) NOT NULL COMMENT 'Пол (0 - ж; 1 - м)',
  `birthday` date NOT NULL COMMENT 'Дата рождения персоны',
  `created_at` datetime NOT NULL COMMENT 'Дата создания рейтинга',
  `since` datetime NOT NULL COMMENT 'Дата с которой проводится текущий подсчет'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Рейтинг';

--
-- Дамп данных таблицы `rating`
--

INSERT INTO `rating` (`id`, `name`, `description`, `gender`, `birthday`, `created_at`, `since`) VALUES
(1, 'Тимофей', 'Дошкольник, группа Одуванчик д/с Огонек', 1, '2015-06-12', '2020-11-09 00:00:00', '2020-11-09 00:00:00'),
(2, 'Елизавета', '2 класс колледжа', 0, '2012-11-30', '2020-11-09 00:00:00', '2020-11-09 00:00:00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Ключ', AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
