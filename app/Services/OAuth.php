<?php

namespace App\Services;

use OAuth2\GrantType\UserCredentials;
use OAuth2\Server;
use OAuth2\Request;
use App\Models\OAuthModel;

class OAuth
{
    public MyServer $server;


    function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $config = array('access_lifetime' => 86400);
        $storage = new MyPdo(array('dsn' => $_ENV['DSN']), array('user_table' => 'users'));
        $grantType = new UserCredentials($storage);
        $this->server = new MyServer($storage, $config);
        $this->server->addGrantType($grantType);
    }

    public function isLoggedIn()
    {
        return $this->server->verifyResourceRequest(Request::createFromGlobals());
    }

}