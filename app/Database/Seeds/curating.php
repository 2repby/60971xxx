<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class curating extends Seeder
{
	public function run()
	{
       $data = [
                
                'name' => 'Тимофей',
                'description'=>'д/с Огонек',
                'gender' => 1,
                'birthday' => '2015-06-12',
                'created_at' => date('Y-m-d H:i:s', time()),
                'since' => date('Y-m-d H:i:s', time()),
                "user_id" => 1,
        ];
        $this->db->table('rating')->insert($data);

        $data = [
            
            'name' => 'Елизавета',
            'description'=>'2 класс Колледжа им. Знаменского',
            'gender' => 0,
            'birthday' => '2012-11-30',
            'created_at' => date('Y-m-d H:i:s', time()),
            'since' => date('Y-m-d H:i:s', time()),
            "user_id" => 1,
        ];
        $this->db->table('rating')->insert($data);

        $data = [
                
                'name'=> 'Выполнение д/з',
                'description'=>'Количество начисляемых быллов зависит от объема задания, скорости и качества выполнения',
                'rate' => '12'
        ];
        $this->db->table('activity_type')->insert($data);

        $data = [
            
            'name'=> 'Домашняя уборка',
            'description'=>'Количество баллов определяется эффективностью уборки',
            'rate' => '6'
        ];
        $this->db->table('activity_type')->insert($data);

        $data = [
            
            'name'=> 'Занятия физкультурой и спортом',
            'description'=>'Спортивные тренировки под руководством тренера или самомтоятельно',
            'rate' => '10'
        ];
        $this->db->table('activity_type')->insert($data);

        $data = [
            
            'name'=> 'Домашка вся',
            'date' => '2021-01-24',
            'activity_type_id' => 1,
            'rate' => '10',
            'rating_id' => 2,
        ];
        $this->db->table('activity')->insert($data);

        $data = [
            
            'name'=> 'Уборка конструктора',
            'date' => '2021-01-25',
            'activity_type_id' => 2,
            'rate' => '5',
            'rating_id' => 2,
        ];
        $this->db->table('activity')->insert($data);

        $data = [
            
            'name'=> 'Разминка',
            'date' => '2021-01-28',
            'activity_type_id' => 3,
            'rate' => '4',
            'rating_id' => 1,
        ];
        $this->db->table('activity')->insert($data);

        $data = [
            
            'name'=> 'Рисование по заданию',
            'description' => 'Нарисовал сиреноголового',
            'date' => '2021-01-30',
            'activity_type_id' => 1,
            'rate' => '4',
            'rating_id' => 1,
        ];
        $this->db->table('activity')->insert($data);


	}
}
