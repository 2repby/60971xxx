<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Curating extends Migration
{
	public function up()
	{
        // activity_type
        if (!$this->db->tableexists('rating'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'description' => array('type' => 'TEXT', 'null' => TRUE),
                'gender' => array('type' => 'INT', 'null' => FALSE),
                'birthday' => array('type' => 'DATE', 'null' => TRUE),
                'created_at' => array('type' => 'TIMESTAMP', 'null' => TRUE),
                'since' => array('type' => 'DATETIME', 'null' => TRUE),
                'user_id' => array('type' => 'int', 'unsigned' => TRUE, 'null' => TRUE),
            ));
            $this->forge->addForeignKey('user_id','users','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('rating', TRUE);
        }

        if (!$this->db->tableexists('activity_type'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'description' => array('type' => 'TEXT', 'null' => FALSE),
                'rate' => array('type' => 'INT', 'null' => FALSE)
            ));
            // create table
            $this->forge->createtable('activity_type', TRUE);
        }

        // activity
        if (!$this->db->tableexists('activity'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'description' => array('type' => 'TEXT', 'null' => TRUE, 'null' => TRUE),
                'date' => array('type' => 'DATE', 'null' => FALSE),
                'rate' => array('type' => 'INT', 'null' => FALSE),
                'activity_type_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => TRUE),
                'rating_id' => array('type' => 'INT', 'unsigned' => TRUE)
            ));
            $this->forge->addForeignKey('activity_type_id','activity_type','id','RESTRICT','RESRICT');
            $this->forge->addForeignKey('rating_id','rating','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('activity', TRUE);
        }
	}

	//--------------------------------------------------------------------

	public function down()
	{

         $this->forge->droptable('activity');
         $this->forge->droptable('rating');
         $this->forge->droptable('activity_type');
	}
}
