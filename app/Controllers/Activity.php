<?php

namespace App\Controllers;

use App\Models\ActivityModel;
use App\Models\RatingModel;


class Activity extends BaseController
{
    public function store()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'name' => 'required|min_length[3]|max_length[255]',
                'date'  => 'required',
                'rate'  => 'required|integer',
                'rating_id' => 'required',
            ]))
        {
            $model = new ActivityModel();
            $model->save([
                'name' => $this->request->getPost('name'),
                'description' => $this->request->getPost('description'),
                'date' => $this->request->getPost('date'),
                'rate' => $this->request->getPost('rate'),
                'rating_id' => $this->request->getPost('rating_id'),
            ]);
            session()->setFlashdata('message', lang('Curating.activity_create_success'));
            return redirect()->to('/rating/view/'.$this->request->getPost('rating_id'));
        }
        else
        {
            return redirect()->to('/rating/view/'.$this->request->getPost('rating_id'))->withInput();
        }
    }
    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new ActivityModel();
        $rating_id = $this->getRatingId($id);
        $model->delete($id);
        return redirect()->to('/rating/view/'.$rating_id);
    }
    public function getRatingId($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new ActivityModel();
        return $model->getRatingId($id);
    }
}