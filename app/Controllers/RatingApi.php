<?php

namespace App\Controllers;

use App\Models\OAuthModel;
use Aws\S3\S3Client;
use CodeIgniter\RESTful\ResourceController;
use App\Models\RatingModel;
use App\Services\OAuth;
use OAuth2\Request;

class RatingApi extends ResourceController
{
    protected $modelName = 'App\Models\RatingModel';
    protected $format = 'json';
    protected $oauth;

    public function rating() //Отображение всех записей
    {
        $this->oauth = new OAuth();
        if ($this->oauth->isLoggedIn()) {
            $OAuthModel = new OAuthModel();
            $model = $this->model;
            $per_page = $this->request->getPost('per_page');
            $search = $this->request->getPost('search');
            $data = $this->model->getRatings($OAuthModel->getUser()->group_name == 'admin' ? null : $OAuthModel->getUser()->id, $search, $per_page);
            //Ответ контроллера включает данные (ratings) и параметры пагинации (pager)
            return $this->respond(['ratings' => $data, 'pager' => $model->pager->getDetails('group1')]);
        } else $this->oauth->server->getResponse()->send();
    }

    public function ratingByUser($id) //Отображение всех записей
    {
        $oauth = new OAuth();
        $OAuthModel = new OAuthModel();
        if ($oauth->isLoggedIn()) {
            return $this->respond($this->model->getRatingByUser(($OAuthModel->getUser()->id)));

        }
        $oauth->server->getResponse()->send();
    }

    public function store()
    {
        $this->oauth = new OAuth();
        if ($this->oauth->isLoggedIn()) {
            $OAuthModel = new OAuthModel();
            $model = $this->model;
            if ($this->request->getMethod() === 'post' && $this->validate([
                    'name' => 'required|min_length[3]|max_length[255]',
                    'description' => 'required',
                    'birthday' => 'required|valid_date[Y-m-d]',
                    'gender' => 'required|in_list[0,1]',
                    'picture' => 'is_image[picture]|max_size[picture,12000]',
                ])) {
                //получение загруженного файла из HTTP-запроса

                $file = $this->request->getFile('picture');
                if ($file->getSize() != 0) {
                    //подключение хранилища
                    $s3 = new S3Client([
                        'version' => 'latest',
                        'region' => 'us-east-1',
                        'endpoint' => getenv('S3_ENDPOINT'),
                        'use_path_style_endpoint' => true,
                        'credentials' => [
                            'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                            'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                        ],
                    ]);
                    //получение расширения имени загруженного файла
                    $ext = explode('.', $file->getName());
                    $ext = $ext[count($ext) - 1];
                    //загрузка файла в хранилище
                    $insert = $s3->putObject([
                        'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                        //генерация уникального имени файла конкатенацией случайного числа и метки времени:
                        'Key' => getenv('S3_KEY') . '/file' . time() . rand(100000, 999999) . '.' . $ext,
                        'Body' => fopen($file->getRealPath(), 'r+')
                    ]);
                }
                //подготовка данных для модели
                $data = [
                    'name' => $this->request->getPost('name'),
                    'description' => $this->request->getPost('description'),
                    'birthday' => $this->request->getPost('birthday'),
                    'gender' => $this->request->getPost('gender'),
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'since' => date('Y-m-d H:i:s', time()),
                    'user_id' => $OAuthModel->getUser()->id,
                ];
                //если изображение было загружено и была получена ссылка на него то добавить ссылку в данные модели
                if (!is_null($insert))
                    $data['picture_url'] = $insert['ObjectURL'];
                $model->save($data);
                //генерация HTTP-ответа с кодом 201 Created
                return $this->respondCreated(null, 'Rating created successfully');
            } else {
                return $this->respond($this->validator->getErrors());
            }
        } else $this->oauth->server->getResponse()->send();

    }

    public function delete($id = null)
    {
        $this->oauth = new OAuth();
        if ($this->oauth->isloggedIn()) {

            $model = new RatingModel();
            $model->delete($id);
            return $this->respondDeleted(null, 'Rating deleted successfully');
        }

        $this->oauth->server->getResponse()->send();
    }

}