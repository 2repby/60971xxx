<?php namespace App\Controllers;

use App\Models\ActivityModel;
use App\Models\RatingModel;
use Aws\S3\S3Client;


class Rating extends BaseController
{
    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form', 'url']);
        $per_page = 2;
        $model = new RatingModel();
        //Обработка запроса на поиск
        if (!is_null($this->request->getPost('search'))) {
            session()->setFlashdata('search', $this->request->getPost('search'));
            $search = $this->request->getPost('search');
        } else {
            $search = session()->getFlashdata('search');
            session()->setFlashdata('search', $search);
            if (is_null($search)) $search = '';
        }
        $data['rating'] = $model->getRatings($this->ionAuth->isAdmin() ? null : $this->ionAuth->user()->row()->id, $search, $per_page);
        $data['search'] = $search;
        $data['pager'] = $model->pager;

        echo view('rating/view_all', $this->withIon($data));
        // var_dump($model->pager->);
        //die();
    }


    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $data ['validation'] = \Config\Services::validation();
        $model = new RatingModel();
        $activity_model = new ActivityModel();
        if (!$this->ionAuth->isAdmin()) //если текущий пользователь не админ
        {
            //если запрашиваемый рейтинг принадлежит текущему пользователю
            if ($model->getRating($id)['user_id'] == $this->ionAuth->user()->row()->id) {
                $data['rating'] = $model->getRating($id);
                $activity = $activity_model->getActivityByRating($id);
            }
        } else $data ['rating'] = $model->getRating($id);
        //подготовка активностей
        $activity = $activity_model->getActivityByRating($id);
        $data['activity'] = [];
        $item = null;
        $flag = false;
        $i = 0;
        foreach ($activity as $item) {
            if (!array_key_exists($item['date'], $data['activity'])) {
                $data['activity'][$item['date']] = [];

                if ($flag) {
                    array_push($data['activity'][$prev], $i);
                    $i = 0;
                }
                $flag = true;

            }
            array_push($data['activity'][$item['date']], array($item['id'], $item['name'], $item['description'], $item['rate']));
            $prev = $item['date'];
            $i = $i + $item['rate'];
        }
        if (!is_null($item)) array_push($data['activity'][$item['date']], $i);
        //Подготовка суммарного рейтинга
        $data['rate'] = $activity_model->getRateByRating($id);
        //Вызов представления
        echo view('rating/view', $this->withIon($data));
    }


    public function store()
    {
        helper(['form', 'url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'name' => 'required|min_length[3]|max_length[255]',
                'description' => 'required',
                'birthday' => 'required',
                'gender' => 'required',
                'picture' => 'is_image[picture]|max_size[picture,12000]',
            ])) {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }
            $model = new RatingModel();
            //подготовка данных для модели
            $data = [
                'name' => $this->request->getPost('name'),
                'description' => $this->request->getPost('description'),
                'birthday' => $this->request->getPost('birthday'),
                'gender' => $this->request->getPost('gender'),
                'created_at' => date('Y-m-d H:i:s', time()),
                'since' => date('Y-m-d H:i:s', time()),
                'user_id' => $this->ionAuth->user()->row()->id,
            ];
            //если изображение было загружено и была получена ссылка на него то даобавить ссылку в данные модели
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            session()->setFlashdata('message', lang('Curating.rating_create_success'));
            return redirect()->to('/rating');
        } else {
            return redirect()->to('/rating/create')->withInput();
        }
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('rating/create', $this->withIon($data));
    }

    public function update()
    {
        helper(['form', 'url']);
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id' => 'required',
                'name' => 'required|min_length[3]|max_length[255]',
                'description' => 'required',
                'birthday' => 'required',
                'gender' => 'required',
                'picture' => 'is_image[picture]|max_size[picture,1024]',
            ])) {
            //получение загруженного файла из HTTP-запроса
            $insert = null;
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }
            $model = new RatingModel();

            $data = [
                'id' => $this->request->getPost('id'),
                'name' => $this->request->getPost('name'),
                'description' => $this->request->getPost('description'),
                'birthday' => $this->request->getPost('birthday'),
                'gender' => $this->request->getPost('gender'),
                'created_at' => date('Y-m-d H:i:s', time()),
                'since' => date('Y-m-d H:i:s', time()),
                'user_id' => $this->ionAuth->user()->row()->id,
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            session()->setFlashdata('message', lang('Curating.rating_update_success'));
            $model->save($data);
            return redirect()->to('/rating');
        } else {
            return redirect()->to('/rating/edit/' . $this->request->getPost('id'))->withInput();
        }
    }


    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new RatingModel();

        helper(['form']);
        $data ['rating'] = $model->getRating($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('rating/edit', $this->withIon($data));

    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new RatingModel();
        $model->delete($id);
        return redirect()->to('/rating');
    }

    public function viewAllWithUsers()
    {
        if ($this->ionAuth->isAdmin()) {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            } else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;
            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search'))) {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            } else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;
            helper(['form', 'url']);
            $model = new RatingModel();
            $data['rating'] = $model->getRatingWithUser(null, $search)->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            echo view('rating/view_all_with_users', $this->withIon($data));
        } else {
            session()->setFlashdata('message', lang('Curating.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
    }

    public function viewWithUser($id)
    {
        $model = new RatingModel();
        $data['rating'] = $model->getRatingWithUser($id);
        /* var_dump($data);
         die();*/
        echo view('rating/view_with_user', $this->withIon($data));
    }

}

