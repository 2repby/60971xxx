<?php

namespace App\Controllers;

use App\Models\OAuthModel;
use App\Services\GoogleClient;
use App\Services\MyPdo;
use App\Services\MyServer;
use App\Services\OAuth;
use OAuth2\GrantType\UserCredentials;
use OAuth2\Request;
use OAuth2\Server;

class OAuthController extends BaseController
{
    private OAuthModel $OAuthModel;
    private OAuth $OAuth;


    public function __construct()
    {
        $this->OAuthModel = new OAuthModel();
        $this->OAuth = new OAuth();
        //Создание клиента с параметром state=frontend для того, чтобы отличать запросы на аутентификацию, пришедшие с
        //frontend-приложения, чтобы делать редирект на главную страницу SPA
        $this->googleClient = new GoogleClient('frontend');
//        Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
//        Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
//        Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method
    }

/// добавлено 22.02.2022
    public function getAuthUrl()
    {
        return $this->googleClient->getGoogleClient()->createAuthUrl();
    }

    public function Authorize()
    {

        $request = Request::createFromGlobals();
        $this->OAuth->server->handleTokenRequest($request)->send();
//        $response = $this->OAuth->server->handleTokenRequest($request);
//        return $response->getResponseBody();

    }

    public function user()
    {
        if ($this->OAuth->isLoggedIn()) {
            return json_encode($this->OAuthModel->getUser());
        } else $this->OAuth->server->getResponse()->send();
    }


    //Метод только для тестирования
    public function oauthtest()
    {
        $token = $this->OAuth->server->getTokenForFrontend('administrator', 'TestClient');
        return json_encode($token);
    }

}
