<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\ContactModel;

class FileUpload extends BaseController
{
    public function __construct()
    {
    }

    public function index()
    {
        return view('pages/uploadform');
    }

    public function upload()
    {

        helper(['form', 'url']);

        $validated = $this->validate([
            'file' => [
                'uploaded[file]',
                'mime_in[file,image/jpg,image/jpeg,image/gif,image/png]',
                'max_size[file,4096]',
            ],
        ]);

        $msg = 'Please select a valid file';

        if ($validated) {
            $avatar = $this->request->getFile('file');
            $avatar->move(WRITEPATH . 'uploads');

            $msg = 'File has been uploaded';
        }

        return redirect()->to(base_url('pages/view/uploadform'))->with('msg', $msg);

    }
}