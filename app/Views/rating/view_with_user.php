<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<?php helper(['form','url']); ?>

<?php $month_tr = array("January" => "Январь",
    "February" => "Февраль",
    "March" => "Март",
    "April" => "Апрель",
    "May" => "Май",
    "June" => "Июнь",
    "July" => "Июль",
    "August" => "Август",
    "September" => "Сентябрь",
    "October" => "Октябрь",
    "November" => "Ноябрь",
    "December" => "Декабрь") ?>

<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($rating)) : ?>
            <div class="card mb-3">
                <div class="row">
                    <div class="col-sm-6 d-flex align-items-center">
                        <?php if (is_null($rating['picture_url'])) : ?>
                            <?php if ($rating['gender'] == 0) : ?>
                                <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/2829/2829841.svg" class="card-img" alt="<?= esc($rating['name']); ?>">
                            <?php else:?>
                                <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/163/163801.svg" class="card-img" alt="<?= esc($rating['name']); ?>">
                            <?php endif ?>
                        <?php else:?>
                            <img height="150" src="<?= esc($rating['picture_url']); ?>" class="card-img" alt="<?= esc($rating['name']); ?>">
                        <?php endif ?>
                    </div>
                    <div class="col-sm-3">
                        <div class="card-body">
                            <h5 class="card-title"><?= esc($rating['name']); ?></h5>
                            <h5 class="card-title"><?= esc($rating['email']); ?></h5>
                            <p class="card-text"><?= esc($rating['description']); ?></p>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Пол:</div>
                                <?php if ($rating['gender'] == 0) : ?>
                                    <div class="text-muted">Женский</div>
                                <?php else:?>
                                    <div class="text-muted">Мужской</div>
                                <?php endif ?>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Дата рождения:</div>
                                <div class="text-muted"><?= date_format(date_create($rating['birthday']),"d.m.Y"); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Рейтинг создан:</div>
                                <div class="text-muted"><?= date_format(date_create($rating['since']),"d.m.Y"); ?></div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>

    <?php else : ?>
        <p>Рейтинг не найден.</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>