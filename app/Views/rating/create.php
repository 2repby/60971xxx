<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('rating/store'); ?>
    <div class="form-group">
        <label for="name">Имя</label>
        <input type="text" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>" name="name"
               value="<?= old('name'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('name') ?>
        </div>

    </div>
    <div class="form-group">
        <label for="name">Описание</label>
        <input type="text" class="form-control <?= ($validation->hasError('description')) ? 'is-invalid' : ''; ?>" name="description"
               value="<?= old('description'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('description') ?>
        </div>

    </div>
    <div class="form-group">
        <label class="form-check-label">Пол</label>
        <div class="form-check ">
            <input class="form-check-input" type="radio" name="gender" value="0" <?= old('gender')=='0' ? 'checked': '' ?> >
            <label class="form-check-label">
                <small class="form-text text-muted">Женский</small>
            </label>
        </div>
        <div class="form-check ">
            <input class="form-check-input" type="radio" name="gender" value="1" <?= old('gender')=='1' ? 'checked': '' ?> >
            <label class="form-check-label">
                <small class="form-text text-muted">Мужской</small>
            </label>

        </div>
        <div class="invalid-feedback" style="display: block">
            <?= $validation->getError('gender') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="birthday">Дата рождения</label>
        <input type="date" class="form-control <?= ($validation->hasError('birthday')) ? 'is-invalid' : ''; ?>" name="birthday" value="<?= old('birthday'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('birthday') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="picture">Изображение</label>
        <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
        <div class="invalid-feedback">
            <?= $validation->getError('picture') ?>
        </div>
    </div>

    <div class="form-group">
    <button type="submit" class="btn btn-primary" name="submit">Создать</button>
    </div>
    </form>

    </div>
<?= $this->endSection() ?>