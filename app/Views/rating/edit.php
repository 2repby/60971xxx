<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('rating/update'); ?>
    <input type="hidden" name="id" value="<?= $rating["id"] ?>">

    <div class="form-group">
        <label for="name">Имя</label>
        <input type="text" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>" name="name"
               value="<?= $rating["name"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('name') ?>
        </div>

    </div>
    <div class="form-group">
        <label for="name">Описание</label>
        <input type="text" class="form-control <?= ($validation->hasError('description')) ? 'is-invalid' : ''; ?>" name="description"
               value="<?= $rating["description"] ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('description') ?>
        </div>

    </div>
    <div class="form-group">
        <label class="form-check-label">Пол</label>
        <div class="form-check ">
            <input class="form-check-input" type="radio" name="gender" value="0" <?= $rating["gender"]=='0' ? 'checked': '' ?> >
            <label class="form-check-label">
                <small class="form-text text-muted">Женский</small>
            </label>
        </div>
        <div class="form-check ">
            <input class="form-check-input" type="radio" name="gender" value="1" <?= $rating["gender"]=='1' ? 'checked': '' ?> >
            <label class="form-check-label">
                <small class="form-text text-muted">Мужской</small>
            </label>

        </div>
        <div class="invalid-feedback" style="display: block">
            <?= $validation->getError('gender') ?>
        </div>
    </div>
        <div class="form-group">
            <label for="birthday">Дата рождения</label>
            <input type="date" class="form-control <?= ($validation->hasError('birthday')) ? 'is-invalid' : ''; ?>" name="birthday" value="<?= $rating["birthday"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('birthday') ?>
            </div>
        </div>
    <div class="form-group">
        <div class="row">

                <div class="col-md-4 d-flex align-items-center">
                    <?php if (is_null($rating['picture_url'])) : ?>
                        <?php if ($rating['gender'] == 0) : ?>
                            <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/2829/2829841.svg" class="card-img" alt="<?= esc($rating['name']); ?>">
                        <?php else:?>
                            <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/163/163801.svg" class="card-img" alt="<?= esc($rating['name']); ?>">
                        <?php endif ?>
                    <?php else:?>
                        <img height="150" src="<?= esc($rating['picture_url']); ?>" class="card-img" alt="<?= esc($rating['name']); ?>">
                    <?php endif ?>
                </div>

            <div class="col">
                <label for="birthday">Изображение</label>
                <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
                <div class="invalid-feedback">
                    <?= $validation->getError('picture') ?>
                </div>
            </div>
        </div>




    </div>
    <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
    </div>
    </form>
    </div>
<?= $this->endSection() ?>