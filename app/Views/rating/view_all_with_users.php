<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">

    <h2>Все рейтинги:</h2>
    <div class="d-flex mb-2">
        <?= $pager->links('group1','my_page') ?>
        <?= form_open('rating/viewAllWithUsers', ['style' => 'display: flex']); ?>
        <select name="per_page" class="ml-3" aria-label="per_page">
            <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
            <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
            <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
            <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
        </select>
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
        </form>
        <?= form_open('rating/viewAllWithUsers',['style' => 'display: flex']); ?>
        <input type="text" class="form-control ml-3" name="search" placeholder="Имя или описание" aria-label="Search"
               value="<?= $search; ?>">
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
        </form>
    </div>

    </div>
    <table class="table table-striped">
        <thead>
            <th scope="col">Аватар</th>
            <th scope="col">Имя</th>
            <th scope="col">Email создателя</th>
            <th scope="col">Описание</th>
            <th scope="col">Управление</th>
            <th scope="col">Рейтинг</th>

        </thead>
        <?php if (!empty($rating) && is_array($rating)) : ?>
        <tbody>
    <?php foreach ($rating as $item): ?>
        <tr>
        <td>
            <?php if (is_null($item['picture_url'])) : ?>
                <?php if ($item['gender'] == 0) : ?>
                    <img height="50" src="https://www.flaticon.com/svg/static/icons/svg/2829/2829841.svg"alt="<?= esc($item['name']); ?>">
                <?php else:?>
                    <img height="50" src="https://www.flaticon.com/svg/static/icons/svg/163/163801.svg" alt="<?= esc($item['name']); ?>">
                <?php endif ?>
            <?php else:?>
                <img height="50" src="<?= esc($item['picture_url']); ?>" alt="<?= esc($item['name']); ?>">
            <?php endif ?>
        </td>
        <td><?= esc($item['name']); ?></td>
        <td><?= esc($item['email']); ?></td>
        <td><?= esc($item['description']); ?></td>
            <td>
                <a href="<?= base_url()?>/rating/view/<?= esc($item['id']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
                <a href="<?= base_url()?>/rating/edit/<?= esc($item['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                <a href="<?= base_url()?>/rating/delete/<?= esc($item['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
            </td>
        <td>199</td>
        </tr>
    <?php endforeach; ?>
        </tbody>
        </table>

<?php else : ?>
    </tbody>
    </table>
    <div class="text-center">
    <p>Рейтинги не найдены </p>
        <a class="btn btn-primary btn-lg" href="<?= base_url()?>/rating/create"><span class="fas fa-tachometer-alt" style="color:white"></span>&nbsp;&nbsp;Создать рейтинг</a>
    </div>
<?php endif ?>
</div>
<?= $this->endSection() ?>