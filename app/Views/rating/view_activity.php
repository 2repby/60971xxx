<?= $this->extend('templates/layout') ?>
<?= $this->section('sub_content') ?>
<div class="container main">
<h2>Все рейтинги</h2>

<?php if (!empty($activity) && is_array($activity)) : ?>

    <?php foreach ($activity as $item): ?>

        <div class="card mb-3" style="max-width: 540px;">
            <div class="row">
                <?= esc($item['date']); ?>

        <?php foreach ($item as $item1): ?>

                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title"><?= esc($item1['name']); ?></h5>
                        <p class="card-text"></p>
                        <a href="<?= base_url()?>/index.php/rating/view/<?= esc($item1['rate']); ?>" class="btn btn-primary">Просмотреть</a>
                        <p class="card-text"><small class="text-muted">199</small></p>
                    </div>
                </div>

        <?php endforeach; ?>

            </div>
        </div>

    <?php endforeach; ?>
<?php else : ?>
    <p>Не найдены активности.</p>
<?php endif ?>
</div>
<?= $this->endSection() ?>