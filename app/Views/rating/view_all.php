<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
<?php if (!empty($rating) && is_array($rating)) : ?>
    <h2>Ваши рейтинги:</h2>
    <div class="d-flex justify-content-between mb-2">
    <?= $pager->links('group1','my_page') ?>

    <?= form_open_multipart('rating',['style' => 'display: flex']); ?>
        <input type="text" class="form-control me-2" name="search" placeholder="Введите имя" aria-label="Search"
               value="<?= $search; ?>">
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
        </form>
    </div>


    <?php foreach ($rating as $item): ?>

        <div class="card mb-3" style="max-width: 540px;">
            <div class="row">
                <div class="col-md-4 d-flex align-items-center">
                    <?php if (is_null($item['picture_url'])) : ?>
                        <?php if ($item['gender'] == 0) : ?>
                            <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/2829/2829841.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                        <?php else:?>
                            <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/163/163801.svg" class="card-img" alt="<?= esc($item['name']); ?>">
                        <?php endif ?>
                    <?php else:?>
                        <img height="150" src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['name']); ?>">
                    <?php endif ?>
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title"><?= esc($item['name']); ?></h5>
                        <p class="card-text"><?= esc($item['description']); ?></p>
                        <a href="<?= base_url()?>/rating/view/<?= esc($item['id']); ?>" class="btn btn-primary">Просмотреть</a>

                            <a href="<?= base_url()?>/rating/edit/<?= esc($item['id']); ?>" class="btn btn-primary">Редактировать</a>
                            <a href="<?= base_url()?>/rating/delete/<?= esc($item['id']); ?>" class="btn btn-danger">Удалить</a>

                        <p class="card-text"><small class="text-muted">199</small></p>
                    </div>
                </div>
            </div>
        </div>

    <?php endforeach; ?>
<?php else : ?>
    <div class="text-center">
    <p>Рейтинги не найдены </p>
        <a class="btn btn-primary btn-lg" href="<?= base_url()?>/rating/create"><span class="fas fa-tachometer-alt" style="color:white"></span>&nbsp;&nbsp;Создать рейтинг</a>
    </div>
<?php endif ?>
</div>
<?= $this->endSection() ?>