<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="jumbotron text-center">
    <img class="mb-4" src="logo.png" alt="" width="72" height="72">
    <h1 class="display-4">CURating</h1>
    <p class="lead">Это приложение поможет создавать расписание активностей и вести учет индивидуальных достижений
        (рейтинг) детей дошкольного и школьного возраста.</p>
    <?php if (!$ionAuth->loggedIn()): ?>

        <a class="btn btn-primary btn-lg" href="<?= base_url() ?>/auth/login"><span class="fas fa fa-sign-in-alt"
                                                                                    style="color:white"></span>&nbsp;&nbsp;Вход</a>
    <?php else: ?>
        <a class="btn btn-primary btn-lg" href="<?= base_url() ?>/rating"><span class="fas fa-shapes"
                                                                                style="color:white"></span>&nbsp;&nbsp;Начать</a>
    <?php endif ?>


</div>
<?= $this->endSection() ?>
