<?php namespace App\Models;
use CodeIgniter\Model;
class UserModel extends Model
{
    protected $table = 'users'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['name', 'description', 'birthday', 'gender', 'created_at', 'since', 'user_id', 'picture_url'];
    public function getRating($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where('id',$id)->first();
    }
    public function getRatingByUser($user_id = null)
    {
        if (!isset($user_id)) {
            return null;
        }
        return $this->where('user_id',$user_id)->findAll();
    }
}