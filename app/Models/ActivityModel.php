<?php namespace App\Models;
use CodeIgniter\Model;
class ActivityModel extends Model
{
    protected $table = 'activity'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['name', 'description', 'date', 'rate', 'activity_type_id', 'rating_id'];
    public function getActivity($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where('id',$id)->first();
    }
    public function getActivityByRating($rating_id = null)
    {
        if (!isset($rating_id)) {
            return null;
        }
        return $this->where('rating_id',$rating_id)->orderBy('date', 'desc')->findAll();
    }
    public function getRateByRating($rating_id = null)
    {
        if (!isset($rating_id)) {
            return null;
        }
        $builder = $this->where('rating_id',$rating_id)->selectSum('rate')->first();
        return $builder;
    }
    public function getRatingId($id = null)
    {
        if (!isset($id)) {
            return null;
        }
        $builder = $this->where('id',$id)->first();
        return $builder['rating_id'];
    }
}