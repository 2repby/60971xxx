<?php

namespace App\Models;

use CodeIgniter\Model;
use Config\Database;
use App\Services\OAuth;
use OAuth2\Request;

class OAuthModel extends Model
{
    protected $oauth;
    protected $table = 'users';

    public function getUser()
    {
        $oauth = new OAuth();
        if ($oauth->isLoggedIn()) {
            $token = $oauth->server->getAccessTokenData(Request::createFromGlobals())['access_token'];
            $query = $this->db->table('users')
                ->select('users.id,users.username,users.email,users.first_name,users.last_name,groups.name as group_name')
                ->join('oauth_access_tokens', 'users.username = oauth_access_tokens.user_id')
                ->join('users_groups', 'users.id = users_groups.user_id')
                ->join('groups', 'groups.id = users_groups.group_id')
                ->where('oauth_access_tokens.access_token', $token)
                ->limit(1)
                ->get();
            // $query = $this->db->table('users')->select('*')->limit(1)->get();
            $user = $query->getRow();

        }

        if (isset($user)) return $user;

    }
}