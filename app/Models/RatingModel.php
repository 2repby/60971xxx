<?php namespace App\Models;

use CodeIgniter\Model;
use Config\Database;

class RatingModel extends Model
{
    protected $table = 'rating'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['name', 'description', 'birthday', 'gender', 'created_at', 'since', 'user_id', 'picture_url'];


    public function getRating($id = null)
    {
        return $this->where('id', $id)->first();
    }

    // Если $user_id == null то возвращаются все рейтинги, иначе - только принадлежащие пользователю с user_id
    public function getRatings($user_id = null, $search = '', $per_page = null)
    {
        $model = $this->like('name', is_null($search) ? '' : $search, 'both');
        if (!is_null($user_id)) {
            $model = $model->where('user_id', $user_id);
        }
        // Пагинация
        return $model->paginate($per_page, 'group1');
    }

//      Пагинация вручную
//    public function getRatings($user_id = null, $search = '', $page = 1, $per_page = null)
//    {
//        $data = $this;
//        if (!is_null($user_id)) {
//            $data = $this->where('user_id', $user_id);
//        }
//        $data = $data->like('name', $search, 'both');
//        if (isset($per_page)) {
//            $data = $data->offset(($page - 1) * $per_page)->limit($per_page);
//        }
//        return $data;
//    }

    public
    function getRatingByUser($user_id = null, $search = null)
    {
        if (!isset($user_id)) {
            return null;
        }
        return $this->where(['user_id' => $user_id])->like('name', $search, 'both')->paginate(2, 'group1');
    }

    public
    function getRatingWithUser($id = null, $search = '')
    {
        $builder = $this->select('*')->join('users', 'rating.user_id = users.id')->like('name', $search, 'both', null, true)->orlike('description', $search, 'both', null, true);
        if (!is_null($id)) {
            return $builder->where(['rating.id' => $id])->first();
        }
        return $builder;
    }
}