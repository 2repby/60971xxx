<?php

/**
 * Number language strings.
 *
 * @package    CodeIgniter
 * @author     CodeIgniter Dev Team
 * @copyright  2014-2019 British Columbia Institute of Technology (https://bcit.ca/)
 * @license    https://opensource.org/licenses/MIT	MIT License
 * @link       https://codeigniter.com
 * @since      Version 4.0.0
 * @filesource
 *
 * @codeCoverageIgnore
 */

return [
    'rating_create_success' => 'Рейтинг был успешно создан',
    'rating_update_success' => 'Рейтинг был успешно обновлен',
    'activity_create_success' => 'Активность была успешно создана',
    'close_modal' => 'Закрыть',
    'login_with_google' => 'Войти с Google',
    'admin_permission_needed' => 'Требуются полномочия администратора'
];
