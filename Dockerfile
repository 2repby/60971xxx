FROM php:7.4-apache
# RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
# RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN curl -sS https://getcomposer.org/installer | php && \
mv composer.phar /usr/local/bin/composer

#RUN echo "DocumentRoot /var/www/html/public" >> /etc/apache2/apache2.conf
#RUN apt-get update && apt-get install -y git unzip zip libxslt-dev libpq-dev zlib1g-dev libicu-dev g++
# RUN apt-get install -y \
#        libzip-dev \
#        zip \
#  && docker-php-ext-install zip
#RUN apt-get update && apt-get install -y libpq-dev
#RUN apt-get install -y zlib1g-dev libicu-dev g++
#&& docker-php-ext-configure intl && docker-php-ext-install intl
#RUN set -ex && apk --no-cache add postgresql-dev
#RUN docker-php-ext-install pdo pdo_pgsql intl

#RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql
#RUN docker-php-ext-install git unzip zip libxslt-dev libpq-dev zlib1g-dev libicu-dev g++
ENV APACHE_DOCUMENT_ROOT /var/www/html/public
ARG DEBIAN_FRONTEND=noninteractive
# Install PDO and PGSQL Drivers
RUN apt-get update && apt-get install -y libpq-dev libicu-dev git unzip zip \
  && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
  && docker-php-ext-install intl pdo pdo_pgsql pgsql

#RUN docker-php-ext-install pdo pdo_pgsql intl
#RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql
# RUN docker-php-ext-install xsl mysqli pdo pdo_mysql
# RUN docker-php-ext-install pdo pdo_pgsql
RUN a2enmod rewrite
COPY 000-default.conf /etc/apache2/sites-available/
COPY . /var/www/html
WORKDIR /var/www/html/
RUN chmod -R o+rwx /var/www/html/writable
RUN echo "ServerName 142.93.172.230" >> /etc/apache2/apache2.conf
RUN service apache2 restart
RUN composer update
RUN composer install
EXPOSE 80 443
