-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Дек 22 2020 г., 17:14
-- Версия сервера: 5.7.32-0ubuntu0.18.04.1
-- Версия PHP: 7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60971xxx`
--

-- --------------------------------------------------------

--
-- Структура таблицы `activity`
--

CREATE TABLE `activity` (
  `id` int(11) NOT NULL COMMENT 'Ключ',
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `activity_type`
--

CREATE TABLE `activity_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'Наименование типа активности',
  `description` text CHARACTER SET utf8 NOT NULL COMMENT 'Описание типа активности'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Структура таблицы `groups1`
--

CREATE TABLE `groups1` (
  `id` int(11) NOT NULL COMMENT 'ID группы',
  `name` varchar(255) NOT NULL COMMENT 'Шифр группы',
  `spec` varchar(255) DEFAULT NULL COMMENT 'Специальность',
  `teacher` varchar(255) DEFAULT NULL COMMENT 'Куратор'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `groups1`
--

INSERT INTO `groups1` (`id`, `name`, `spec`, `teacher`) VALUES
(1, '609-71', '09.03.04 Программная инженерия', 'Кузин Д.А.'),
(2, '605-71', '27.03.04 Управление в технических системах', 'Запевалов А.В.');

-- --------------------------------------------------------

--
-- Структура таблицы `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `rating`
--

CREATE TABLE `rating` (
  `id` int(11) NOT NULL COMMENT 'Ключ',
  `name` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'Имя персоны',
  `description` text CHARACTER SET utf8 NOT NULL COMMENT 'Описание',
  `gender` int(11) NOT NULL COMMENT 'Пол (0 - ж; 1 - м)',
  `birthday` date NOT NULL COMMENT 'Дата рождения персоны',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Дата создания рейтинга',
  `since` datetime NOT NULL COMMENT 'Дата с которой проводится текущий подсчет'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Рейтинг';

--
-- Дамп данных таблицы `rating`
--

INSERT INTO `rating` (`id`, `name`, `description`, `gender`, `birthday`, `created_at`, `since`) VALUES
(1, 'Тимофей Кузин', 'Дошкольник, группа Одуванчик д/с Огонек', 1, '2015-06-12', '2020-12-21 17:42:59', '2020-11-09 00:00:00'),
(2, 'Елизавета', '2 класс колледжа', 0, '2012-11-30', '2020-11-08 19:00:00', '2020-11-09 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL COMMENT 'ID студента',
  `name` varchar(255) NOT NULL COMMENT 'ФИО',
  `birthdate` date DEFAULT NULL COMMENT 'Дата рождения',
  `id_group` int(11) DEFAULT NULL COMMENT 'ID группы'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `students`
--

INSERT INTO `students` (`id`, `name`, `birthdate`, `id_group`) VALUES
(1, 'Иванов', '2001-10-21', NULL),
(2, 'Петров', '1998-10-14', 2),
(3, 'Сидоров', '2020-10-19', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$12$xH9AWbEJdARCQmLv.towh.hLE0fkTvzBbxfyUhvG7KTdoDhri8ESe', 'admin@admin.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1608633802, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(2, '127.0.0.1', '2repby@gmail.com', '$2y$10$T7XHx.ImTPrwrfAGS/VOEei/3RNZXxfQUao8Nkj55JijqffeHlOXa', '2repby@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1605475739, 1605476276, 1, 'Дмитрий', 'Кузин', 'СурГУ', '+79222588216'),
(3, '127.0.0.1', 'kuzin_da@surgu.ru', '$2y$10$HjpPjfYpsoWmuIUpLXe88O.nsDqVx56Q.tWNLXRgP/Z8XhGApvYmy', 'kuzin_da@surgu.ru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1606405168, 1606413656, 1, 'Дмитрий', 'Кузин', 'СурГУ', '+79222588216'),
(4, '127.0.0.1', 'surgut20a@gmail.com', '$2y$10$Z48ks8Lxn9Md85wS.oMXWuwxBJrXv2HSFwbWwGJ5UP/avo1hJ66A6', 'surgut20a@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1606564876, 1606564894, 1, 'Иван', 'Иванов', NULL, '+79222588216'),
(5, '127.0.0.1', 'admin@example.com', '$2y$10$/3BxgNqulRNdAiAgy6XDp.PsakpqQXVjdBrwudm6NL0S7QHR10y5K', 'admin@example.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1607499556, NULL, 1, 'dsf', 'dg', NULL, 'dfh');

-- --------------------------------------------------------

--
-- Структура таблицы `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(4, 3, 2),
(5, 4, 2),
(6, 5, 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `groups1`
--
ALTER TABLE `groups1`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_group` (`id_group`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_email` (`email`),
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`);

--
-- Индексы таблицы `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `groups1`
--
ALTER TABLE `groups1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID группы', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Ключ', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID студента', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_ibfk_1` FOREIGN KEY (`id_group`) REFERENCES `groups1` (`id`);

--
-- Ограничения внешнего ключа таблицы `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
